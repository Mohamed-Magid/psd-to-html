$(document).ready(function () {
    // Appear Of Scroll Top button
    $(window).on("scroll", function () {
        "use strict";
        if ($(window).scrollTop() > 500) {
            $('.toTop').fadeIn();
        } else {
            $('.toTop').fadeOut();
        }
    });

    // Scroll To Top Button
    $(".toTop").click(function () {
        "use strict";
        $('html, body').animate({
console.log(html)
            scrollTop: $(".header").offset().top
        }, 1000);
    });
    // Move to the Next Section
    $(".header button").click(function () {
        "use strict";
        $('html, body').animate({
            scrollTop: $(".intro").offset().top
        }, 1000);
    });
    // Move active class
    $(".slider span").click(function () {
        "use strict";
        $(this).addClass("sliderActive").siblings().removeClass("sliderActive");
    });
});
