$(document).ready(function () {

    ////////////////////////////////////////////
    /**** Make Navbar Fixed When Scroll on ****/
    ///////////////////////////////////////////
    var fixed = "navbar-fixed-top",
        spacenav = $("nav").offset().top;

    $(window).scroll(function () {
        "use strict";
        var space = $(window).scrollTop();

        if (space >= spacenav) {

            $("nav").addClass(fixed);
            fixed = "";

            $("nav").css("box-shadow", "0px 7px 15px #807f7f");

            $("body").css("padding-top", $(".navbar").height() + 15);

        } else {
            $("nav").removeClass("navbar-fixed-top");

            fixed = "navbar-fixed-top";

            $("nav").css("box-shadow", "none");

            $("body").css("padding-top", "0");
        }
    });
    /**************************************/



    /////////////////////////////
    /**** Learn More Button ****/
    ////////////////////////////
    $(".banner button").click(function () {
        "use strict";
        $("body,html").animate({
            scrollTop: $("nav").offset().top
        }, 1000);
    });


    $(".navbar-default .navbar-nav > li a").click(function () {
        "use strict";
        var hrefClicked = $(this).attr("href");
        $(this).parent().addClass("active").siblings().removeClass("active");

        $("body,html").animate({
            scrollTop: $(hrefClicked).offset().top - 57
        }, 1000);
    });
    /**************************************/

    $(window).scroll(function () {
        if ($(window).scrollTop() > 700) {
            $(".top").fadeIn(500);
        } else {
            $(".top").fadeOut(500);
        }
    });

    $(".top").click(function () {
        $("body,html").animate({
            scrollTop: 0
        }, 1000);
    });

});

// Loading Screen
$(window).load(function () {
    $(".loading .spinner").delay(3000).fadeOut(1000, function () {
        $(".loading").fadeOut(1000, function () {
            $("body").css("overflow", "auto");
            $(".loading").remove();
        });
    });
});
